#include "header.h"
using namespace std;

int main()
{
	srand(time(0));
	setlocale(0, "ru");
	cout << "������� ������ �������: ";
	short matrixSize; 
	cin >> matrixSize;
	system("cls");

	vector<string> menu = { "������� ������� ", "����������� �������", "����� �������� ����� ���� ������������� � ������������ ��������� ������ ������", "��������� �������������� �������", "����� ������ ������������� ������� ��� �������� ���������� �������", "����� ��������� ������ �������", "�������� ��� ������������� �������� ������ 5", "�������� ������� ������, ���������� ������������ � ����������� ��������", "���������� ���������� ��������� �����", "�����" };
	unsigned short item;
	int** matrix = NULL;

	do
	{
		DrawMenu(menu);
		cin >> item;
		system("cls");

		switch (item)
		{
		case 1:
			matrix = CreateMatrix(matrixSize);
			break;
		case 2:
			PrintMatrix(matrix, matrixSize);
			break;
		case 3:
			PrintDifferenceBetweenSumOfNumsOfMaxAndMinValueAtRow(matrix, matrixSize);
			break;
		case 4:
			cout << ((IsMatrixSymmetric(matrix, matrixSize)) ? "������� �����������" : "������� �������������");
			break;
		case 5:
			cout << "������ ������������ ����� ��� �������� ����������: " << FindFirstPositiveValueUnderDiagonal(matrix, matrixSize);
			break;
		case 6:
			cout << "����� ��������� ������ �������: " << GetSumOfElementsOfEvenColumns(matrix, matrixSize);
			break;
		case 7:
		{
			ReplaceAllNegativeElementsWithFive(matrix, matrixSize); //����� �����, �.�. ���� ������� �� ������� �� ����� �������� � ��� �������� �������� (����� �� 4 ������ ��������)
			break;
		}
		case 8:
		{
			SwapMaxAndMinContainingRows(matrix, matrixSize);
			cout << "������, ���������� ������������ � ����������� ������� ���������� �������.";
			break;
		}
		case 9:
			cout << "���������� ��������� �����: " << GetCountOfNonZeroRows(matrix, matrixSize);
			break;
		case 10:
			exit(0);
			break;
		}
		cout << endl << "������� ����� �������, ����� ��������� � ����...";
		char exitSymbol = _getch();
		system("cls");
	} while (item != menu.size());
	return 0;
}