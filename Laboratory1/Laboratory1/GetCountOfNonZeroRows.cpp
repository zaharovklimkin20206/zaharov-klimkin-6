#include <iostream>
using namespace std;
int GetCountOfNonZeroRows(int** matrix, short size)
{
	if (!matrix)
	{
		cout << "������� �������� ������.";
	}
	else
	{
		int nonZeroRows = 0;
		for (int i = 0; i < size; i++)
		{
			for (int j = 0; j < size; j++)
			{
				if (matrix[i][j] != 0)
				{
					nonZeroRows += 1;
					break;
				}
			}
		}
		return nonZeroRows;
	}
	return -1;
}