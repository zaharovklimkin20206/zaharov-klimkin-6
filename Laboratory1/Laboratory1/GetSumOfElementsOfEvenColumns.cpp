#include <iostream>
using namespace std;
int GetSumOfElementsOfEvenColumns(int** matrix, short size)
{
	if (!matrix)
	{
		cout << "������� �������� �������!";
		return -1;
	}
	else
	{
		int sum = 0;
		for (int i = 0; i < size; i++)
		{
			if ((i % 2) == 1)
			{
				continue;
			}
			for (int j = 0; j < size; j++)
			{
				sum += matrix[j][i];
			}
		}
		return sum;
	}
}