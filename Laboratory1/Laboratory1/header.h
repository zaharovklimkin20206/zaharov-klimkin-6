#pragma once
#ifndef _LAB1_
#define _LAB1_

#include <iostream>
#include <time.h>
#include <vector>
#include <conio.h>

const int MAX_RANDOM_VALUE = 10;	// ������� ������� ���������� �����
const int MIN_RANDOM_VALUE = -10;	// ������� ������� ���������� �����

void DrawMenu(std::vector<std::string> list);	// ��������� ��������� ����
int** CreateMatrix(short);	// �������� ������� ���������� �������
void PrintMatrix(int**, short);	// ����� ������� �� �����
void PrintDifferenceBetweenSumOfNumsOfMaxAndMinValueAtRow(int**, short); // ����� �������� ����� ���� ������������� � ������������ ����� � ������ ������ �������
bool IsMatrixSymmetric(int**, short); // ���������, �������� �� ������� ������������
int FindFirstPositiveValueUnderDiagonal(int**, short); // ���������� ������ ������������� ������� ��� ����������
int GetSumOfElementsOfEvenColumns(int**, short); // �������� ����� ��������� ������ �������
void ReplaceAllNegativeElementsWithFive(int**, short); // �������� ��� ������������� ����� ������ 5
void SwapMaxAndMinContainingRows(int**, short); // ������ ������� ������, ���������� ������������ � ����������� �������
int GetCountOfNonZeroRows(int**, short); // �������� ���������� ��������� �����
#endif