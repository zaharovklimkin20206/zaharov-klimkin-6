#include <iostream>
#include "header.h"
void SwapMaxAndMinContainingRows(int** matrix, short size)
{
	int max = MIN_RANDOM_VALUE;
	int min = MAX_RANDOM_VALUE;
	int rowContainMax = 0;
	int rowContainMin = 0;
	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
			if (matrix[i][j] > max)
			{
				max = matrix[i][j];
				rowContainMax = i;
			}
			if (matrix[i][j] < min)
			{
				min = matrix[i][j];
				rowContainMin = i;
			}
		}
	}
	for (int i = 0; i < size; i++)
	{
		std::swap(matrix[rowContainMax][i], matrix[rowContainMin][i]);
	}
}